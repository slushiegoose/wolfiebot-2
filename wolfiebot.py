import discord
from discord.ext import commands
import os
import traceback

startup_extensions = [
    "jishaku",
    "game",
    "generatelist",
    "in_game_roles",
    "roles",
    "events",
    "misc",
    "links",
    "stats",
    "redirect"
]

try:
    from config import TOKEN
except ModuleNotFoundError:
    TOKEN = os.environ["TOKEN"]


intents=  discord.Intents.default()
intents.members = True
bot = commands.Bot(command_prefix=["w.", "W."], owner_id=131131701148647424, intents=intents)


@bot.event
async def on_ready():
    print("Ready!")
    await bot.get_channel(513432525750927362).send("BOT ONLINE!")
    await bot.change_presence(activity=discord.Game(name="w.help"))
    for extension in startup_extensions:
        try:
            p = f"cogs.{extension}" if extension != "jishaku" else extension
            bot.load_extension(p)
            #  print(f"Loaded cogs.{extension}")
        except Exception:
            print(f"Failed to load extension cogs.{extension}")
            traceback.print_exc()


if __name__ == "__main__":
    bot.run(TOKEN)
