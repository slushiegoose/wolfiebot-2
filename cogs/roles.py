import discord
from discord.ext import commands
from cogs.lib import (
    UVoteEmojis, embeds, InverseCommands, roleslist, VoteEmojis,
    AllRoles, Alignments, Categories, Species, Factions,
    UniqueRoles, AchievableRoles, Modifiers, AchievableModifiers, Icons,
    archive
)
import cogs.lib.archive.embeds
import asyncio
import random
from fuzzywuzzy.process import extractOne


class Roles(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    async def getroles(self, conditions):
        All = list(AllRoles + Modifiers)
        Valid = list(All)
        ref = {
            "good": [r for r in AllRoles if Alignments[r] == "G"],
            "evil": [r for r in AllRoles if Alignments[r] == "E"],
            "neutral": [r for r in AllRoles if Alignments[r] == "N"],
            "chaos": [r for r in AllRoles if "Ch" in Categories[r]],
            "counteractive": [r for r in AllRoles if "Co" in Categories[r]],
            "investigative": [r for r in AllRoles if "I" in Categories[r]],
            "killing": [r for r in AllRoles if "K" in Categories[r]],
            "protective": [r for r in AllRoles if "P" in Categories[r]],
            "support": [r for r in AllRoles if "S" in Categories[r]],
            "human": [r for r in AllRoles if "Human" == Species[r]],
            "arcane": [r for r in AllRoles if "Arcane" == Species[r]],
            "ethereal": [r for r in AllRoles if "Ethereal" == Species[r]],
            "unearthly": [r for r in AllRoles if "Unearthly" == Species[r]],
            "wolf": [r for r in AllRoles if "Wolf" == Species[r]],
            "unique": [r for r in AllRoles if r in UniqueRoles],
            "achievable": [
                r for r in All if r in AchievableRoles + AchievableModifiers
            ],
            "modifier": Modifiers, "x-modifier": AllRoles,
            "coven": [r for r in All if "Coven" in Factions[r]],
            "prophets": [r for r in All if "Prophets" in Factions[r]],
            "school": [r for r in All if "School" in Factions[r]],
            "troupe": [r for r in All if "Troupe" in Factions[r]],
            "vampiric": [r for r in All if "Vampiric" in Factions[r]],
            "witches": [r for r in All if "Witches" in Factions[r]],
            "wolves": [r for r in All if "Wolves" in Factions[r]],
            "x-good": [r for r in AllRoles if Alignments[r] != "G"],
            "x-evil": [r for r in AllRoles if Alignments[r] != "E"],
            "x-neutral": [r for r in AllRoles if Alignments[r] != "N"],
            "x-chaos": [r for r in AllRoles if "Ch" not in Categories[r]],
            "x-counteractive": [
                r for r in AllRoles if "Co" not in Categories[r]
            ],
            "x-investigative": [
                r for r in AllRoles if "I" not in Categories[r]
            ],
            "x-killing": [r for r in AllRoles if "K" not in Categories[r]],
            "x-protective": [
                r for r in AllRoles if "P" not in Categories[r]
            ],
            "x-support": [r for r in AllRoles if "S" not in Categories[r]],
            "x-human": [r for r in AllRoles if "Human" != Species[r]],
            "x-arcane": [r for r in AllRoles if "Arcane" != Species[r]],
            "x-ethereal": [r for r in AllRoles if "Ethereal" != Species[r]],
            "x-unearthly": [
                r for r in AllRoles if "Unearthly" != Species[r]
                ],
            "x-wolf": [r for r in AllRoles if "Wolf" != Species[r]],
            "x-unique": [r for r in AllRoles if r not in UniqueRoles],
            "x-achievable": [
                r for r in All if r not in (
                    AchievableRoles + AchievableModifiers
                )
            ],
            "x-coven": [r for r in All if "Coven" not in Factions[r]],
            "x-prophets": [r for r in All if "Prophets" not in Factions[r]],
            "x-school": [r for r in All if "School" not in Factions[r]],
            "x-troupe": [r for r in All if "Troupe" not in Factions[r]],
            "x-vampiric": [r for r in All if "Vampiric" not in Factions[r]],
            "x-witches": [r for r in All if "Witches" not in Factions[r]],
            "x-wolves": [r for r in All if "Wolves" not in Factions[r]],
            "o-chaos": [r for r in AllRoles if ["Ch"] == Categories[r]],
            "o-counteractive": [
                r for r in AllRoles if ["Co"] == Categories[r]
            ],
            "o-investigative": [
                r for r in AllRoles if ["I"] == Categories[r]
            ],
            "o-killing": [r for r in AllRoles if ["K"] == Categories[r]],
            "o-protective": [r for r in AllRoles if ["P"] == Categories[r]],
            "o-support": [r for r in AllRoles if ["S"] == Categories[r]],
            "o-coven": [r for r in All if ["Coven"] == Factions[r]],
            "o-prophets": [r for r in All if ["Prophets"] == Factions[r]],
            "o-school": [r for r in All if ["School"] == Factions[r]],
            "o-troupe": [r for r in All if ["Troupe"] == Factions[r]],
            "o-vampiric": [r for r in All if ["Vampiric"] == Factions[r]],
            "o-witches": [r for r in All if ["Witches"] == Factions[r]],
            "o-wolves": [r for r in All if ["Wolves"] == Factions[r]]}
        if conditions != ["all"]:
            for r in All:
                for c in conditions:
                    if c in ref:
                        if r not in ref[c]:
                            Valid.remove(r)
                            break
        return Valid

    async def mutliEmbed(self, ctx, embed_list, message=""):
        home, back, forward, end = '⏮', '◀', '▶', '⏭'
        stop = '⏹'
        shuffle = "🔀"
        valid_r = [home, back, forward, end, shuffle, stop]
        page = 0
        max_page = len(embed_list)
        msg = await ctx.send(
            message+embed_list[page][0],
            embed=embed_list[page][1])
        for i in valid_r:
            await msg.add_reaction(i)
        await asyncio.sleep(0.1)
        try:
            while True:
                reaction, user = await self.bot.wait_for(
                    'reaction_add',
                    check=(lambda r, u: (
                        r.emoji in valid_r and
                        r.message.id == msg.id
                    )),
                    timeout=120
                )
                try:
                    await msg.remove_reaction(reaction, user)
                except Exception:
                    pass
                if reaction.emoji == home:
                    page = 0
                elif reaction.emoji == back:
                    page -= 1
                elif reaction.emoji == forward:
                    page += 1
                elif reaction.emoji == end:
                    page = max_page - 1
                elif reaction.emoji == shuffle:
                    page = random.randint(1, max_page)
                elif reaction.emoji == stop:
                    break

                page %= max_page
                await msg.edit(
                    content=message+embed_list[page][0],
                    embed=embed_list[page][1]
                )
        except Exception:
            pass
        await msg.delete()

    async def multiContent(self, ctx, content_list, message=""):
        home, back, forward, end = '⏮', '◀', '▶', '⏭'
        stop = '⏹'
        valid_r = [home, back, forward, end, stop]
        page = 0
        max_page = len(content_list)
        msg = await ctx.send(message+"\n"+content_list[page])
        for i in valid_r:
            await msg.add_reaction(i)
        await asyncio.sleep(0.1)
        try:
            while True:
                reaction, user = await self.bot.wait_for(
                    'reaction_add',
                    check=(lambda r, u: (
                        r.emoji in valid_r and
                        r.message.id == msg.id
                    )),
                    timeout=120
                )
                try:
                    await msg.remove_reaction(reaction, user)
                except Exception:
                    pass
                if reaction.emoji == home:
                    page = 0
                elif reaction.emoji == back:
                    page -= 1
                elif reaction.emoji == forward:
                    page += 1
                elif reaction.emoji == end:
                    page = max_page - 1
                elif reaction.emoji == stop:
                    break

                page %= max_page
                await msg.edit(content=message+"\n"+content_list[page])
        except Exception:
            pass
        await msg.delete()

    @commands.command(aliases=["r", "role"])
    async def roles(self, ctx, *, role: str, where: discord.TextChannel = None):
        if not where:
            where = ctx.channel
        name = role.lower().replace(" ", "").replace("ō", "o")
        if name in archive.ArchiveInverseCommands:
            return await ctx.send("Role has been deleted & archived.")
        fuzzied, index = extractOne(name, [y for y in dir(embeds) if not y.startswith("_")])
        if index <= 75:
            return await ctx.send("Error. Role doesn't exist.")
        try:
            with open(f"roles/{fuzzied}.png","rb") as f:
                file = discord.File(f, filename=f"{fuzzied}.png")
            embed = getattr(embeds, fuzzied)
            embed.set_thumbnail(url=f"attachment://{fuzzied}.png")
            await where.send(
                f"**{InverseCommands[fuzzied]}**",
                embed=embed,
                file=file
            )
        except FileNotFoundError:
            await where.send(
                f"**{InverseCommands[fuzzied]}**",
                embed = getattr(embeds, fuzzied)
            )

    @commands.command(aliases=["rr"])
    async def randomrole(self, ctx, *conditions):
        if len(conditions) == 0:
            conditions = ["all"]
        conditions = list(map(lambda x: x.lower(), conditions))
        Valid = await self.getroles(conditions)
        if Valid == []:
            await ctx.send("No roles exist that fit all parameters, sorry.:(")
        else:
            await ctx.send(f"**{random.choice(Valid)}**")

    @commands.command(aliases=["rl"])
    async def rolelist(self, ctx):
        await self.multiContent(ctx, roleslist)

    @commands.command(aliases=["lr"])
    async def listroles(self, ctx, *conditions):
        conditions = list(map(lambda x: x.lower(), conditions))
        Valid = await self.getroles(conditions)
        if Valid == []:
            await ctx.send("No roles exist that fit all parameters, sorry.:(")
        else:
            display = "{} roles found:\n```".format(len(Valid))
            for r in Valid:
                display = "{} - {}\n".format(display, r)
            display = "{}```".format(display)
            await ctx.send(display)

    @commands.command(aliases=["wr","wtr"])
    async def walkthroughroles(self, ctx, *conditions):
        if len(conditions) == 0:
            conditions = ["all"]
        else:
            conditions = list(map(lambda x: x.lower(), conditions))
        Valid = await self.getroles(conditions)
        if Valid == []:
            await ctx.send("No roles exist that fit all parameters, sorry.:(")
        else:
            embeds_needed = []
            for role in Valid:
                name = role.lower().replace(" ", "").replace("ō", "o")
                embeds_needed.append([f"**{role}**", getattr(embeds, name)])
            await self.mutliEmbed(
                ctx,
                embeds_needed,
                "{} roles found:\n".format(len(Valid))
            )

    @commands.command()
    async def icon(self, ctx, *, role):
        role = role.lower().replace(" ", "")
        try:
            with open(f"roles/{role}.png", "rb") as f:
                await ctx.send(file=discord.File(f))
        except FileNotFoundError:
            await ctx.send(Icons[role])

    @commands.command()
    async def achieve(self, ctx, *, role):
        role = role.title()
        role = (
            "TARDIS Engineer" if role == "Tardis Engineer"
            else "Rōjinbi" if role == "Rojinbi"
            else role
        )
        te = True if role == "TARDIS Engineer" else False
        if role in AllRoles or role in Modifiers or te:
            methods = {
                "Archfey": (
                    "- Use *Ascend* as a Pixie"
                ),
                "Bloodhound": (
                    "- Be targeted with *Fangs* as a Wolf "
                    "or a player with the Feral modifier.\n"
                    "- Be targeted with both *Fangs* and "
                    "*Infect*.\n"
                    "- Transform as a Drunk with the Direwolf alive"
                    "or with Evil chosen as the alignment."
                ),
                "Cultist": (
                    "- Become Evil as a Priest.\n"
                    "- Have any Priest become a Cultist as a Priest."
                ),
                "Cyberhound": (
                    "- Be targeted with *Infect* as an Inventor.\n"
                    "- Successfully predit *Infect* as a Psychic.\n"
                    "- Transform as a Drunk with the Direwolf alive"
                    "or with Evil chosen as the alignment."
                ),
                "Dodomeki": (
                    "- Become Evil as a Thief or a Rogue.\n"
                    "- Transform as a Drunk with the Direwolf alive"
                    "or with Evil chosen as the alignment."
                ),
                "Hacker": (
                    "- Be targeted with *Investigate* as an Inventor.\n"
                    "- Target a player with *Research* and have at least "
                    "half of the living players (rounded up) appear "
                    "in the results.\n"
                    "- Be targeted with *Deprogram* as a player who started "
                    " the game as a Drunk\n"
                    "- Transform as a Drunk with the Direwolf dead"
                    "and Seer alive or with Good chosen as the alignment."
                ),
                "Hangman": (
                    "- Switch alignments while having the Romantic modifier"
                ),
                "Inventor": (
                    "- Lose the Companion modifer as a TARDIS Engineer."
                ),
                "Jester": (
                    "- Be targeted with *Laughing Gas* if "
                    "targeted with *Lockjaw* the previous night."
                ),
                "Paladin": (
                    "- Live until NIGHT 5 as a Priest.\n"
                    "Transform as a Drunk with the Direwolf dead"
                    "and Seer alive or with Good chosen as the alignment."
                ),
                "Priest": (
                    "- Become Good as a Cultist.\n"
                    "- Have any Cultist become a Priest as a Cultist."
                ),
                "Psychic": (
                    "- Target Evil players with *Loyalty* for three "
                    "nights in a row as an Agent."
                ),
                "Romantic": (
                    "- Switch alignments while having the Hangman modifier"
                ),
                "Shifter": "- Be targeted with *Shift*.",
                "Slasher": (
                    "- Have the Slasher die after being targeted with "
                    "*Legacy*."
                ),
                "Souleater": (
                    "- Spend three nights as a Shifter. "
                    "These nights do not need to be consecutive.\n"
                ),
                "Spy": (
                    "- Target Good players with *Loyalty* "
                    "for three nights in a row as an Agent."
                ),
                "TARDIS Engineer": (
                    "- Be targeted with *Invite* as an Inventor, "
                    "a Companion or a Clockmaker.\n"
                    "- Be targeted with *Invite* by two Time Lords of the "
                    "same alignment."
                ),
                "Vampire": (
                    "- Be targeted with *Fang* while being without any saves."
                ),
                "Warlock": (
                    "- Have any Priest become a Paladin as a Cultist.\n"
                    "- Transform as a Drunk with the Direwolf alive"
                    "or with Evil chosen as the alignment."
                ),
                "Companion": "- Be targeted with *Invite*.",
                "Minstrel": "- (Random) Be in a game with a Bard.",
                "Spectre": (
                    "- Die as an Arsonist.\n"
                    "- Die as a Sylph.\n"
                    "- Be killed by Wolves as a Spider.\n"
                    "- Be killed by a Soulless player as a Souleater.\n"
                    "- Die before the Night you transform as a Drunk\n"
                    "- Die during the same night or the day after being "
                    "targeted with *Curse*.\n"
                    "- Redirect a Poltergeist who is redirecting you as a "
                    "Poltergeist.\n"
                    "- Have your Twin gain the Spectre modifier as a Twin.\n"
                    "- Die by any means other than Suicide or Lynching as a"
                    "Whisperer.\n"
                    "- Be targeted with *Find Familiar*."
                ),
                "Stand User": "- Be targeted with *Stand Arrow*.",
                "Werewolf": "- Use a save gained through *Infect*.",
                "Witch": (
                    "- Be targeted with *Poison* and *Heal* "
                    "on the same night by different players.\n"
                    "- Be targeted with *Find Familiar*."
                )
            }
            try:
                await ctx.send("{}:\n{}".format(role, methods[role]))
            except KeyError:
                await ctx.send((
                    "There are no alternate ways of gaining "
                    "that role or modifier."
                ))

    @commands.command()
    async def vote(self, ctx, *, message="", where="", needed=0, time=0):
        if message == "":
            await ctx.send("""Usage of command <w.vote>:
    ```md
    <w.vote (options to vote between seperated by commas)>
    Example: <w.vote 1, 2, 3>
    Output: 'React with appropriate emoji to vote:
    :A: --> 1
    :B: --> 2
    :C: --> 3'```""")
        else:
            if where == "":
                where = ctx.message.channel
            if type(message) == list:
                options = message
            else:
                options = message.split(", ")
            if len(options) > 20:
                await ctx.send("Too many!")
            else:
                options = sorted(options)
                display = ""
                for i in range(0, len(options)):
                    display = display+(
                        "\n{} --> {}".format(VoteEmojis[i], options[i])
                    )
                vote_message = await where.send(embed=discord.Embed(
                    title="React with appropriate emoji to vote:",
                    description=display
                ))
                if needed != 0 and time != 0:
                    wintimer = 0
                    for i in range(0, time):
                        vote_message = await where.fetch_message(vote_message.id)
                        await vote_message.edit(embed=discord.Embed(
                            title=(
                                "React with appropriate emoji to vote: "
                                "[{}]".format(time-i)
                            ),
                            description=display
                        ))
                        await asyncio.sleep(1)
                        if i % 5 == 0:
                            votes = {
                                VoteEmojis[i]: [0, options[i]]
                                for i in range(0, len(options))
                            }
                            reacts = vote_message.reactions
                            for r in reacts:
                                for i in range(0, len(options)):
                                    if (
                                        r.emoji.encode('utf-8')
                                        == UVoteEmojis[i]
                                    ):
                                        votes[VoteEmojis[i]][0] = r.count
                            top = []
                            for v in votes:
                                if votes[v][0] == max([
                                    votes[i][0] for i in votes
                                ]) and votes[v][0] >= needed:
                                    top.append(votes[v])
                            if top != []:
                                if wintimer == 6:
                                    await vote_message.edit(
                                        embed=discord.Embed(
                                            title=(
                                                "React with appropriate "
                                                "emoji to vote:"
                                            ),
                                            description=display))
                                    if len(top) == 1:
                                        await where.send(
                                            "**{}** has been voted!".format(
                                                top[0][1]
                                            )
                                        )
                                        return top[0]
                                    else:
                                        for w in top:
                                            if w == top[0]:
                                                l_ = "**{}**".format(w[1])
                                            elif w != top[len(top)-1]:
                                                l_ = "{}, **{}**".format(
                                                    l_, w[1]
                                                )
                                            else:
                                                l_ = (
                                                    "{} and **{}** have"
                                                    "been voted!"
                                                ).format(l_, w[1])
                                        await where.send(l_)
                                        # solve tie
                                        return
                                else:
                                    wintimer = wintimer + 1
                            else:
                                wintimer = 0
                    vote_message = await where.fetch_message(vote_message.id)
                    await vote_message.edit(
                        embed=discord.Embed(
                            title="React with appropriate emoji to vote:",
                            description=display
                        )
                    )
                    votes = {
                        VoteEmojis[i]: [0, options[i]] for i in range(
                            0, len(options)
                        )
                    }
                    reacts = vote_message.reactions
                    for r in reacts:
                        for i in range(0, len(options)):
                            if r.emoji.encode('utf-8') == UVoteEmojis[i]:
                                votes[VoteEmojis[i]][0] = r.count
                    top = []
                    for v in votes:
                        if votes[v][0] == max([votes[i][0] for i in votes]):
                            top.append(votes[v])
                    if len(top) == 1:
                        await where.send("**{}** has been voted!".format(
                            top[0][1]
                        ))
                        return top[0]
                    else:
                        for w in top:
                            if w == top[0]:
                                l_ = "**{}**".format(w[1])
                            elif w != top[len(top)-1]:
                                l_ = "{}, **{}**".format(l_, w[1])
                            else:
                                l_ = "{} and **{}** have been voted!".format(
                                    l_, w[1]
                                )
                        await where.send(l_)
                        # solve tie
                        return

    @commands.command(aliases=["avote"])
    async def advancedvote(self, ctx, *, message="", where=""):
        if message == "":
            await ctx.send("""Usage of command <w.advancedvote>:
    ```md
    <w.advancedvote (options to vote between seperated by commas; time; \
    needed)>
    Example: <w.vote 1, 2, 3; 60; 3>
    Output: 'React with appropriate emoji to vote:
   :A: --> 1
   :B: --> 2
   :C: --> 3'
    The vote will last 60 seconds (or until any option gets 3 votes) and will\
    then return the result.```""")
        else:
            if where == "":
                where = ctx.message.channel
            message = message.split("; ")
            time = int(message[1])
            needed = int(message[2])
            message = message[0]
            await ctx.invoke(
                self.vote,
                message=message,
                time=time,
                needed=needed
            )

    @commands.command(aliases=["s"])
    async def search(self, ctx, *, criteria):
        embed_descs = [
            (e, getattr(embeds, e).description.lower().replace("*",""))
            for e in dir(embeds)
            if e not in ["discord","icons"]
            and not e.startswith("_")
        ]
        crit = criteria.lower()

        Valid = [InverseCommands[r] for r, d in embed_descs if crit in d]
        if Valid == []:
            await ctx.send("Search term not found... Sorry :(")
        else:
            display = "{1} found in {0} roles:\n```".format(len(Valid), criteria)
            for r in Valid:
                display = "{} - {}\n".format(display, r)
            display = "{}```".format(display)
            await ctx.send(display)
        return Valid

    @commands.command(aliases=["a"])
    async def action(self, ctx, *, action):
        criteria = f"{action} -"
        old_send = ctx.send
        ctx.send = asyncio.coroutine(lambda *i: None)
        Valid = await ctx.invoke(self.search, criteria=criteria)
        ctx.send = old_send
        if Valid == []:
            await ctx.send("Action not found... Sorry :(")
        elif len(Valid) == 1:
            await ctx.send(f"**{action.title()}** is the action belonging to the **{Valid[0]}**.")
        else:
            display = "{1} found in {0} roles:\n```".format(len(Valid), criteria)
            for r in Valid:
                display = "{} - {}\n".format(display, r)
            display = "{}```".format(display)
            await ctx.send(display)



def setup(bot):
    bot.add_cog(Roles(bot))
