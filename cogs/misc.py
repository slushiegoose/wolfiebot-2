from discord.ext import commands
import random


class Misc(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def flip(self, ctx, num: int = 1):
        results = []
        for _ in range(num):
            results.append(random.choice(["H", "T"]))
        await ctx.send(f"**{', '.join(results)}**")

    @commands.command(aliases=["choice"])
    async def randomchoice(self, ctx, *, choices):
        choices = choices.split(", ")
        await ctx.send(f"**{random.choice(choices)}**")

    @commands.command()
    async def score(self, ctx, W, L):
        formula = (100+(W+L)*2)*(W-L)*(1+(W+1)/(W+L+1))
        await ctx.send(f"**{formula}**")


def setup(bot):
    bot.add_cog(Misc(bot))
