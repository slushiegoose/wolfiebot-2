PLACEHOLDERICON = "https://via.placeholder.com/256x256"
Icons = {'agent': 'https://i.imgur.com/GBbzP40.png', 'alchemist': 'https://i.imgur.com/CkcPTXj.png', 'anarchist': PLACEHOLDERICON, 'apothecary': PLACEHOLDERICON, 'archfey': PLACEHOLDERICON, 'arsonist': 'https://i.imgur.com/eACFT2J.png', 'assassin': PLACEHOLDERICON, 'backstabber': 'https://i.imgur.com/bHh6fmw.png', 'bard': 'https://i.imgur.com/4vqgI1l.png', 'baykok': 'https://i.imgur.com/5aeVRZs.png', 'bloodhound': 'https://i.imgur.com/Ln1TEDm.png', 'clockmaker': 'https://i.imgur.com/9sRU1wY.png', 'companion': 'https://i.imgur.com/jdN1QwN.png', 'cultist': 'https://i.imgur.com/f6b61vM.png', 'cyberhound': 'https://i.imgur.com/EzCRujB.png', 'dentist': 'https://i.imgur.com/HBu6XXy.png', 'direwolf': 'https://i.imgur.com/1ZLzSrI.png', 'doctor': 'https://i.imgur.com/d7nawSg.png', 'dodomeki': 'https://i.imgur.com/niarSn0.png', 'drunk': 'https://i.imgur.com/fx1zfEP.png', 'feral': 'https://i.imgur.com/vmOUm8A.png',  'gladiator': 'https://i.imgur.com/qSxDXwc.png', 'glazier': 'https://i.imgur.com/n7N7dOI.png', 'hacker': 'https://i.imgur.com/IT4TDsx.png', 'hangman': 'https://i.imgur.com/nIlRgYj.png', 'heir': 'https://i.imgur.com/xSQdjo7.png', 'hooligan': 'https://i.imgur.com/HgtdzD8.png', 'hunter': 'https://i.imgur.com/91EYf4h.png', 'inventor': 'https://i.imgur.com/uyVCtUE.png', 'jailor': 'https://i.imgur.com/w1lT9VF.png',  'knight': 'https://i.imgur.com/80i6sbg.png', 'kresnik': 'https://i.imgur.com/l5qO6ik.png', 'lich': PLACEHOLDERICON, 'lonewolf': PLACEHOLDERICON, 'mage': 'https://i.imgur.com/Shp4BCk.png', 'medium': 'https://i.imgur.com/LyVnqOB.png', 'minstrel': 'https://i.imgur.com/wiUUTKk.png', 'morty': 'https://i.imgur.com/KDys0gU.png', 'noir': 'https://i.imgur.com/jK5qG4G.png', 'paladin': 'https://i.imgur.com/d21IPWL.png', 'pixie': 'https://i.imgur.com/wK4dnew.png', 'politician': 'https://i.imgur.com/c96wBhg.png', 'poltergeist': 'https://i.imgur.com/Kw00X8Y.png', 'poser': 'https://i.imgur.com/hAH4Bpq.png', 'priest': 'https://i.imgur.com/hBt3nbH.png', 'prince': 'https://i.imgur.com/WlHqAWN.png', 'psychic': 'https://i.imgur.com/ij227Zp.png', 'researcher': 'https://i.imgur.com/EKYuHHB.png', 'rogue': 'https://i.imgur.com/Mv4kPmv.png', 'romantic': 'https://i.imgur.com/uyGYo8v.png', 'santa': 'https://i.imgur.com/4BbwtSM.png', 'scarecrow': 'https://i.imgur.com/AussjT7.png', 'scavenger': PLACEHOLDERICON, 'seer': 'https://i.imgur.com/ZBw7bqy.png', 'sentinel': 'https://i.imgur.com/pFJIwQq.png', 'sharpshooter': 'https://i.imgur.com/Hi8LZXq.png', 'shifter': 'https://i.imgur.com/OPdTNMZ.png', 'slasher': 'https://i.imgur.com/V2Z0g2x.png', 'souleater': 'https://i.imgur.com/aCewTdC.png', 'soulless': 'https://i.imgur.com/SrXMQaf.png', 'spectre': 'https://i.imgur.com/CLtiWTl.png', 'spider': 'https://i.imgur.com/V5Ovqe9.png', 'spy': 'https://i.imgur.com/HBr0JPO.png', 'standuser': 'https://i.imgur.com/ANrLfnT.png', 'survivalist': 'https://i.imgur.com/Ua7WMU8.png', 'tardisengineer': 'https://i.imgur.com/EdItCwm.png', 'thief': 'https://i.imgur.com/CnqKHwS.png', 'timelord': 'https://i.imgur.com/msxarpT.png', 'twin': 'https://i.imgur.com/jKI4GnP.png', 'vampire': 'https://i.imgur.com/QvxToyj.png', 'warlock': 'https://i.imgur.com/1pWGWgF.png', 'werewolf': 'https://i.imgur.com/SeJ1Fv1.png', 'whisperer': 'https://i.imgur.com/l8c7un3.png', 'witch': 'https://i.imgur.com/uzkYewk.png'} 
InverseCommands = {'agent': 'Agent','alchemist': 'Alchemist', 'anarchist': 'Anarchist', 'arsonist': 'Arsonist', 'assassin': 'Assassin', 'apothecary': 'Apothecary', 'archfey': 'Archfey', 'backstabber': 'Backstabber', 'bard': 'Bard', 'baykok': 'Baykok', 'bloodhound': 'Bloodhound', 'clockmaker': 'Clockmaker', 'cultist': 'Cultist', 'cyberhound': 'Cyberhound', 'dentist': 'Dentist', 'direwolf': 'Direwolf', 'doctor': 'Doctor', 'dodomeki': 'Dodomeki', 'drunk': 'Drunk',  'gladiator': 'Gladiator', 'glazier': 'Glazier', 'hacker': 'Hacker', 'heir': 'Heir', 'hooligan': 'Hooligan', 'hunter': 'Hunter', 'inventor': 'Inventor', 'jailor': 'Jailor',  'knight': 'Knight', 'kresnik': 'Kresnik', 'lich': 'Lich', 'lonewolf': 'Lone Wolf', 'mage': 'Mage', 'medium': 'Medium', 'noir': 'Noir', 'paladin': 'Paladin', 'pixie': 'Pixie', 'politician': 'Politician', 'poltergeist': 'Poltergeist', 'poser': 'Poser', 'priest': 'Priest', 'prince': 'Prince', 'psychic': 'Psychic', 'researcher': 'Researcher', 'rogue': 'Rogue', 'santa': 'Santa', 'scarecrow': 'Scarecrow', 'scavenger': 'Scavenger', 'seer': 'Seer', 'sentinel': 'Sentinel', 'sharpshooter': 'Sharpshooter', 'shifter': 'Shifter', 'slasher': 'Slasher', 'souleater': 'Souleater', 'soulless': 'Soulless', 'spider': 'Spider', 'spy': 'Spy', 'survivalist': 'Survivalist',  'tardisengineer': 'TARDIS Engineer', 'thief': 'Thief', 'timelord': 'Time Lord', 'vampire': 'Vampire', 'warlock': 'Warlock', 'werewolf': 'Werewolf', 'whisperer': 'Whisperer', 'witch': 'Witch', 'companion': 'Companion', 'feral': 'Feral', 'hangman': 'Hangman', 'minstrel': 'Minstrel', 'morty': 'Morty', 'romantic': 'Romantic', 'spectre': 'Spectre', 'standuser': 'Stand User', 'twin': 'Twin'} 
AllRoles = ['Agent', 'Alchemist', 'Anarchist', 'Apothecary', 'Archfey', 'Arsonist', 'Assassin', 'Backstabber', 'Bard', 'Baykok', 'Bloodhound', 'Clockmaker', 'Cultist', 'Cyberhound', 'Dentist', 'Direwolf', 'Doctor', 'Dodomeki', 'Drunk',  'Gladiator', 'Glazier', 'Hacker', 'Heir', 'Hooligan', 'Hunter', 'Inventor', 'Jailor',  'Knight', 'Kresnik', 'Lich', 'Lone Wolf', 'Mage', 'Medium', 'Noir', 'Paladin', 'Pixie', 'Politician', 'Poltergeist', 'Poser', 'Priest', 'Prince', 'Psychic', 'Researcher', 'Rogue', 'Santa', 'Scarecrow', 'Scavenger', 'Seer', 'Sentinel', 'Sharpshooter', 'Shifter', 'Slasher', 'Souleater', 'Spider', 'Spy', 'Survivalist', 'TARDIS Engineer', 'Thief', 'Time Lord', 'Vampire', 'Warlock', 'Werewolf', 'Whisperer', 'Witch'] 
Alignments = {'Agent': 'N', 'Alchemist': 'N', 'Anarchist': 'N', 'Apothecary': 'G', 'Archfey': 'G', 'Arsonist': 'N', 'Assassin': 'N', 'Backstabber': 'E', 'Bard': 'N', 'Baykok': 'E', 'Bloodhound': 'E', 'Clockmaker': 'N', 'Cultist': 'E', 'Cyberhound': 'E', 'Dentist': 'E', 'Direwolf': 'E', 'Doctor': 'G', 'Dodomeki': 'E', 'Drunk': 'N', 'Gladiator': 'G', 'Glazier': 'G', 'Hacker': 'G', 'Heir': 'E', 'Hooligan': 'E', 'Hunter': 'G', 'Inventor': 'N', 'Jailor': 'G',  'Knight': 'G', 'Kresnik': 'G', 'Lich': 'E', 'Lone Wolf': 'N', 'Mage': 'G', 'Medium': 'G', 'Noir': 'G', 'Paladin': 'G', 'Pixie': 'G', 'Politician': 'E', 'Poltergeist': 'E', 'Poser': 'G', 'Priest': 'G', 'Prince': 'G', 'Psychic': 'E', 'Researcher': 'G', 'Rogue': 'G', 'Santa': 'N', 'Scarecrow': 'N', 'Scavenger': 'G', 'Seer': 'G', 'Sentinel': 'N', 'Sharpshooter': 'G', 'Shifter': 'N', 'Slasher': 'N', 'Souleater': 'N', 'Spider': 'E', 'Spy': 'G', 'Survivalist': 'N', 'TARDIS Engineer': 'G', 'Thief': 'E', 'Time Lord': 'G', 'Vampire': 'E', 'Warlock': 'E', 'Werewolf': 'E', 'Whisperer': 'G', 'Witch': 'N'} 
Categories = {'Agent': ['Ch', 'P'], 'Alchemist': ['Ch'], 'Anarchist': ['Ch, K'], 'Apothecary': ['K', 'P'], 'Archfey': ['P','S'], 'Arsonist': ['Ch', 'K'], 'Assassin': ['K'], 'Backstabber': ['Co', 'K'], 'Bard': ['Ch', 'K'], 'Baykok': ['Co', 'K'], 'Bloodhound': ['K', 'S'], 'Clockmaker': ['K'], 'Cultist': ['Co', 'S'], 'Cyberhound': ['Co', 'K'], 'Dentist': ['Co'], 'Direwolf': ['K', 'S'], 'Doctor': ['K', 'P'], 'Dodomeki': ['I'], 'Drunk': ['Ch'],  'Gladiator': ['Co', 'K'], 'Glazier': ['Co'], 'Hacker': ['I', 'S'], 'Heir': ['K'], 'Hooligan': ['K', 'S'], 'Hunter': ['K', 'P'], 'Inventor': ['Ch', 'K'], 'Jailor': ['Co', 'P'],  'Knight': ['K', 'S'], 'Kresnik': ['I', 'K'], 'Lich': ['K'], 'Lone Wolf': ['Co','K'], 'Mage': ['Ch', 'I'], 'Medium': ['S'], 'Noir': ['I', 'K'], 'Paladin': ['Co', 'P'], 'Pixie': ['Co', 'I'], 'Politician': ['K', 'Co'], 'Poltergeist': ['Ch', 'I'], 'Poser': ['S'], 'Priest': ['Co', 'S'], 'Prince': ['S'], 'Psychic': ['Ch', 'S'], 'Researcher': ['I'], 'Rogue': ['Co', 'P'], 'Santa': ['S'], 'Scarecrow': ['Ch', 'S'], 'Scavenger': ['Ch'], 'Seer': ['I'], 'Sentinel': ['P'], 'Sharpshooter': ['I', 'K'], 'Shifter': ['Ch'], 'Slasher': ['Ch', 'K'], 'Souleater': ['Ch', 'K'], 'Spider': ['Co', 'S'], 'Spy': ['I'], 'Survivalist': ['Co'],  'TARDIS Engineer': ['P', 'S'], 'Thief': ['Ch', 'Co'], 'Time Lord': ['I', 'S'], 'Vampire': ['S'], 'Warlock': ['Ch', 'K'], 'Werewolf': ['K'], 'Whisperer': ['I', 'S'], 'Witch': ['K', 'P']} 
Species = {'Agent': 'Human', 'Alchemist': 'Arcane', 'Anarchist': 'Human', 'Apothecary': 'Human', 'Archfey': 'Ethereal', 'Arsonist': 'Human', 'Assassin': 'Human', 'Backstabber': 'Human', 'Bard': 'Human', 'Baykok': 'Ethereal', 'Bloodhound': 'Wolf', 'Clockmaker': 'Human', 'Cultist': 'Human', 'Cyberhound': 'Wolf', 'Dentist': 'Human', 'Direwolf': 'Wolf', 'Doctor': 'Human', 'Dodomeki': 'Unearthly', 'Drunk': 'Human', 'Gladiator': 'Human', 'Glazier': 'Human', 'Hacker': 'Human', 'Heir': 'Human', 'Hooligan': 'Human', 'Hunter': 'Human', 'Inventor': 'Human', 'Jailor': 'Human',  'Knight': 'Human', 'Kresnik': 'Arcane', 'Lich': 'Ethereal', 'Lone Wolf': 'Wolf', 'Mage': 'Arcane', 'Medium': 'Arcane', 'Noir': 'Human', 'Paladin': 'Arcane', 'Pixie': 'Unearthly', 'Politician': 'Human', 'Poltergeist': 'Ethereal', 'Poser': 'Human', 'Priest': 'Human', 'Prince': 'Human', 'Psychic': 'Arcane', 'Researcher': 'Human', 'Rogue': 'Human', 'Santa': 'Unearthly', 'Scarecrow': 'Ethereal', 'Scavenger': 'Human', 'Seer': 'Arcane', 'Sentinel': 'Unearthly', 'Sharpshooter': 'Human', 'Shifter': 'Ethereal', 'Slasher': 'Unearthly', 'Souleater': 'Ethereal', 'Spider': 'Unearthly', 'Spy': 'Human', 'Survivalist': 'Human', 'TARDIS Engineer': 'Human', 'Thief': 'Human', 'Time Lord': 'Unearthly', 'Vampire': 'Unearthly', 'Warlock': 'Arcane', 'Werewolf': 'Wolf', 'Whisperer': 'Arcane', 'Witch': 'Unearthly'} 
Factions = {'Agent': [], 'Alchemist': [], 'Anarchist': [], 'Apothecary': [], 'Archfey': [], 'Arsonist': [], 'Assassin': [], 'Backstabber': [], 'Bard': ['Troupe'], 'Baykok': [], 'Bloodhound': ['Vampiric', 'Wolves'], 'Clockmaker': [], 'Cultist': [], 'Cyberhound': ['Wolves'], 'Dentist': [], 'Direwolf': ['Wolves'], 'Doctor': [], 'Dodomeki': [], 'Drunk': [], 'Gladiator': [], 'Glazier': [], 'Hacker': [], 'Heir': [], 'Hooligan': [], 'Hunter': [], 'Inventor': [], 'Jailor': [],  'Knight': [], 'Kresnik': [], 'Lich': [], 'Lone Wolf': [], 'Mage': [], 'Medium': [], 'Noir': [], 'Paladin': [], 'Pixie': [], 'Politician': [], 'Poltergeist': [], 'Poser': [], 'Priest': [], 'Prince': [], 'Psychic': [], 'Researcher': [], 'Rogue': [], 'Santa': [], 'Scarecrow': [], 'Scavenger': [], 'Seer': [], 'Sentinel': [], 'Sharpshooter': [], 'Shifter': [], 'Slasher': [], 'Souleater': [], 'Spider': [], 'Spy': [], 'Survivalist': [], 'TARDIS Engineer': ['Tardis'], 'Thief': [], 'Time Lord': ['Tardis'], 'Vampire': ['Vampiric'], 'Warlock': [], 'Werewolf': ['Wolves'], 'Whisperer': [], 'Witch': ['Witches'], 'Companion': ['Tardis'], 'Feral': [], 'Hangman': [], 'Minstrel': ['Troupe'], 'Morty': [], 'Romantic': [], 'Soulless': [], 'Spectre': [], 'Stand User': [], 'Twin': []} 
Modifiers = ['Companion', 'Feral', 'Hangman', 'Minstrel', 'Morty', 'Romantic', 'Soulless', 'Spectre', 'Stand User', 'Twin'] 
AchievableRoles = ['Archfey', 'Bloodhound', 'Cyberhound', 'Dodomeki', 'Hacker', 'Paladin', 'Souleater', 'TARDIS Engineer', 'Warlock'] 
UniqueRoles = ['Alchemist', 'Anarchist', 'Apothecary', 'Arsonist', 'Backstabber', 'Bard', 'Baykok', 'Bloodhound', 'Cultist', 'Cyberhound', 'Dentist', 'Direwolf', 'Dodomeki', 'Drunk', 'Gladiator', 'Heir', 'Inventor', 'Jailor', 'Knight', 'Kresnik', 'Lich', 'Lone Wolf', 'Noir', 'Paladin', 'Politician', 'Priest', 'Prince', 'Psychic', 'Researcher', 'Rogue', 'Santa', 'Scarecrow', 'Sharpshooter', 'Slasher', 'Spider', 'TARDIS Engineer', 'Thief', 'Vampire', 'Warlock', 'Whisperer'] 
AchievableModifiers = ['Companion', 'Minstrel', 'Soulless', 'Spectre'] 

roleslist = ["""```md
[+][Agent] - Neutral, Chaos/Protective, Human - <w.roles_agent>
[+][Alchemist] - Neutral, Chaos/Support, Arcane, Unique - <w.roles_alchemist>
[+][Anarchist] - Neutral, Chaos/Killing, Human, Unique - <w.roles_anarchist>
[+][Apothecary] - Good, Killing/Protective, Human, Unique - <w.roles_apothecary>
[+][Archfey] - Good, Protective/Support, Ehtereal, Achievable - <w.roles_archfey>
[+][Arsonist] - Neutral, Killing, Human, Unique - <w.roles_arsonist>
[+][Assassin] - Neutral, Killing, Human - <w.roles_assassin>
[+][Backstabber] - Evil, Counteractive/Killing, Human, Unique - <w.roles_backstabber>
[+][Bard] - Neutral, Chaos/Killing, Human, Unique - <w.roles_bard>
[+][Baykok] - Evil, Counteractive/Killing, Ethereal, Unique - <w.roles_baykok>
[+][Bloodhound] - Evil, Killing/Support, Wolf, Unique, Achievable - <w.roles_bloodhound>
[+][Clockmaker] - Neutral, Killing, Human - <w.roles_clockmaker>
[+][Companion] - Modifier, Achievable - <w.roles_companion>
[+][Cultist] - Evil, Counteractive/Support, Human, Unique - <w.roles_cultist>
[+][Cyberhound] - Evil, Counteractive/Killing, Wolf, Unique, Achievable - <w.roles_cyberhound>
[+][Dentist] - Evil, Counteractive, Human, Unique - <w.roles_dentist>
[+][Direwolf] - Evil, Killing/Support, Wolf, Unique - <w.roles_direwolf>
[+][Doctor] - Good, Killing/Protective, Human - <w.roles_doctor>
[+][Dodomeki] - Evil, Investigative, Unearthly, Unique, Achievable - <w.roles_dodomeki>
[+][Drunk] - Neutral, Chaos, Human, Unique - <w.roles_drunk>
[+][Feral] - Modifier - <w.roles_feral>
[+][Gladiator] - Good, Counteractive/Killing, Human, Unique - <w.roles_gladiator>
[+][Glazier] - Good, Counteractive, Human - <w.roles_glazier>
```""","""```md
[+][Hacker] - Good, Investigative/Support, Human, Unique, Achievable - <w.roles_hacker>
[+][Hangman] - Modifier - <w.roles_hangman>
[+][Heir] - Evil, Killing, Human, Unique - <w.roles_heir>
[+][Hooligan] - Evil, Killing/Support, Human - <w.roles_hooligan>
[+][Hunter] - Good, Killing/Protective, Human - <w.roles_hunter>
[+][Inventor] - Neutral, Chaos/Killing, Human, Unique - <w.roles_inventor>
[+][Jailor] - Good, Counteractive/Protective, Human, Unique - <w.roles_jailor>
[+][Knight] - Good, Killing/Support, Human, Unique - <w.roles_knight>
[+][Kresnik] - Good, Investigative/Killing, Arcane, Unique - <w.roles_kresnik>
[+][Lich] - Evil, Killing, Ethereal, Unique - <w.roles_lich>
[+][Lone Wolf] - Neutral, Counteractive/Killing, Wolf, Unique - <w.roles_lonewolf>
[+][Mage] - Good, Chaos/Investigative, Arcane - <w.roles_mage>
[+][Medium] - Good, Support, Arcane - <w.roles_medium>
[+][Minstrel] - Modifier, Achievable - <w.roles_minstrel>
[+][Morty] - Modifier - <w.roles_morty>
[+][Noir] - Good, Investigative/Killing, Human, Unique - <w.roles_noir>
```""","""```md
[+][Paladin] - Good, Counteractive/Protective, Arcane, Unique, Achievable - <w.roles_paladin>
[+][Pixie] - Good, Counteractive/Investigative, Unearthly - <w.roles_pixie>
[+][Politician] - Evil, Killing/Counteractive, Human, Unique - <w.roles_politician>
[+][Poltergeist] - Evil, Chaos/Investigative, Ethereal - <w.roles_poltergeist>
[+][Poser] - Good, Support, Human - <w.roles_poser>
[+][Priest] - Good, Counteractive/Support, Human, Unique - <w.roles_priest>
[+][Prince] - Good, Support, Human, Unique - <w.roles_prince>
[+][Psychic] - Evil, Chaos/Support, Arcane, Unique - <w.roles_psychic>
[+][Researcher] - Good, Investigative, Human, Unique - <w.roles_researcher>
[+][Rogue] - Good, Counteractive/Protective, Human, Unique - <w.roles_rogue>
[+][Romantic] - Modifier - <w.roles_romantic>
[+][Santa] - Neutral, Support, Unearthly, Unique - <w.roles_santa>
[+][Scarecrow] - Neutral, Chaos/Support, Ethereal, Unique - <w.roles_scarecrow>
[+][Scavenger] - Good, Chaos, Human - <w.roles_scavenger>
[+][Seer] - Good, Investigative, Arcane - <w.roles_seer>
[+][Sentinel] - Neutral, Protective, Unearthly - <w.roles_sentinel>
[+][Sharpshooter] - Good, Investigative/Killing, Human, Unique - <w.roles_sharpshooter>
[+][Shifter] - Neutral, Chaos, Ethereal - <w.roles_shifter>
[+][Slasher] - Neutral, Chaos/Killing, Unearthly, Unique - <w.roles_slasher>
[+][Souleater] - Neutral, Chaos/Killing, Ethereal, Achievable - <w.roles_souleater>
```""","""```md
[+][Soulless] - Modifier, Achievable - <w.roles_soulless>
[+][Spectre] - Modifier, Achievable - <w.roles_spectre>
[+][Spider] - Evil, Counteractive/Support, Unearthly, Unique - <w.roles_spider>
[+][Spy] - Good, Investigative, Human - <w.roles_spy>
[+][Stand User] - Modifier - <w.roles_standuser>
[+][Survivalist] - Neutral, Counteractive, Human - <w.roles_survivalist>
[+][TARDIS Engineer] - Good, Protective/Support, Human, Unique, Achievable - <w.roles_tardisengineer>
[+][Thief] - Neutral, Chaos/Counteractive, Human, Unique - <w.roles_thief>
[+][Time Lord] - Good, Investigative/Support, Unearthly - <w.roles_timelord>
[+][Twin] - Modifier - <w.roles_twin>
[+][Vampire] - Evil, Support, Unearthly, Unique - <w.roles_vampire>
[+][Warlock] - Evil, Chaos/Killing, Arcane, Unique, Achievable - <w.roles_warlock>
[+][Werewolf] - Evil, Killing, Wolf - <w.roles_werewolf>
[+][Whisperer] - Good, Investigative/Support, Arcane, Unique - <w.roles_whisperer>
[+][Witch] - Neutral, Killing/Protective, Unearthly - <w.roles_witch>
```"""]




VoteEmojis = [":regional_indicator_a:", ":regional_indicator_b:", ":regional_indicator_c:", ":regional_indicator_d:", ":regional_indicator_e:", ":regional_indicator_f:",
              ":regional_indicator_g:", ":regional_indicator_h:", ":regional_indicator_i:", ":regional_indicator_j:", ":regional_indicator_k:", ":regional_indicator_l:",
              ":regional_indicator_m:", ":regional_indicator_n:", ":regional_indicator_o:", ":regional_indicator_p:", ":regional_indicator_q:", ":regional_indicator_r:",
              ":regional_indicator_s:", ":regional_indicator_t:", ":regional_indicator_u:", ":regional_indicator_v:", ":regional_indicator_w:", ":regional_indicator_x:",
              ":regional_indicator_y:", ":regional_indicator_z:"]

UVoteEmojis = [b'\xf0\x9f\x87\xa6', b'\xf0\x9f\x87\xa7', b'\xf0\x9f\x87\xa8', b'\xf0\x9f\x87\xa9', b'\xf0\x9f\x87\xaa', b'\xf0\x9f\x87\xab', b'\xf0\x9f\x87\xac', b'\xf0\x9f\x87\xad',
               b'\xf0\x9f\x87\xae', b'\xf0\x9f\x87\xaf', b'\xf0\x9f\x87\xb0', b'\xf0\x9f\x87\xb1', b'\xf0\x9f\x87\xb2', b'\xf0\x9f\x87\xb3', b'\xf0\x9f\x87\xb4', b'\xf0\x9f\x87\xb5',
               b'\xf0\x9f\x87\xb6', b'\xf0\x9f\x87\xb7', b'\xf0\x9f\x87\xb8', b'\xf0\x9f\x87\xb9', b'\xf0\x9f\x87\xba', b'\xf0\x9f\x87\xbb', b'\xf0\x9f\x87\xbc', b'\xf0\x9f\x87\xbd',
               b'\xf0\x9f\x87\xbe', b'\xf0\x9f\x87\xbf']
