import discord


PLACEHOLDERICON = "https://via.placeholder.com/256x256"
ArchiveIcons = {'shinigami': 'https://i.imgur.com/g3lHNHK.png', 'glitch': PLACEHOLDERICON, 'fate': 'https://i.imgur.com/zWwrrc6.png', 'herald': 'https://i.imgur.com/42kAlx1.png', 'inevitable': 'https://i.imgur.com/PlzPjU7.png', 'spinster': 'https://i.imgur.com/VKdzrRc.png', 'hermit': 'https://i.imgur.com/WaMFV6G.png', 'harbinger': 'https://i.imgur.com/7mA9c6F.png', 'emissary': 'https://i.imgur.com/jR48TJh.png', 'page': 'https://i.imgur.com/zbRiee0.png', 'guide': 'https://i.imgur.com/6C59lhY.png', 'rojinbi': 'https://i.imgur.com/l5FG3fd.png', 'conduit': 'https://i.imgur.com/Qapubfn.png', 'philanthropist': 'https://i.imgur.com/UMSqiC4.png', 'merchant': 'https://i.imgur.com/94e1SoH.png', 'maid': 'https://i.imgur.com/j3ls0Bq.png', 'understudy': 'https://i.imgur.com/xtL1C6F.png', 'speedster': 'https://i.imgur.com/2NdRPVX.png', 'sylph': 'https://i.imgur.com/AaFsJ7j.png', 'geneticist': 'https://i.imgur.com/VcOvFfU.png', 'jester': 'https://i.imgur.com/fHcSdG1.png',} 
ArchiveInverseCommands = {'shinigami': 'Shinigami', 'glitch': 'Glitch', 'fate': 'Fate', 'herald': 'Herald', 'inevitable': 'Inevitable', 'spinster': 'Spinster', 'hermit': 'Hermit', 'harbinger': 'Harbinger', 'emissary': 'Emissary', 'page': 'Page', 'guide': 'Guide', 'rojinbi': 'Rōjinbi', 'conduit': 'Conduit', 'philanthropist': 'Philanthropist', 'merchant': 'Merchant', 'maid': 'Maid', 'understudy': 'Understudy', 'speedster': 'Speedster', 'sylph': 'Sylph', 'geneticist': 'Geneticist', 'jester': 'Jester',} 
ArchiveAllRoles = ['Shinigami', 'Glitch', 'Fate', 'Herald', 'Inevitable', 'Spinster', 'Hermit', 'Harbinger', 'Emissary', 'Page', 'Rōjinbi', 'Philanthropist', 'Merchant', 'Maid', 'Understudy', 'Sylph', 'Geneticist', 'Jester',] 
ArchiveAlignments = {'Shinigami': 'E', 'Glitch': 'N', 'Fate': 'N', 'Herald': 'N', 'Inevitable': 'N', 'Spinster': 'N', 'Hermit': 'N', 'Harbinger': 'N', 'Emissary': 'N', 'Page': 'N', 'Rōjinbi': 'N', 'Philanthropist': 'N', 'Merchant': 'N', 'Maid': 'N', 'Understudy': 'N', 'Sylph': 'G', 'Geneticist': 'N', 'Jester': 'E', } 
ArchiveCategories = {'Shinigami': ['Co', 'K'], 'Glitch': ['Ch'], 'Fate': ['Ch', 'S'], 'Herald': ['Ch', 'K'], 'Inevitable': ['K'], 'Spinster': ['S'], 'Hermit': ['Co', 'I'], 'Harbinger': ['Ch', 'K'], 'Emissary': ['P', 'S'], 'Page': ['S'], 'Rōjinbi': ['Ch'], 'Philanthropist': ['Co', 'S'], 'Merchant': ['Ch', 'S'], 'Maid': ['Ch', 'K'], 'Understudy': ['Ch', 'S'], 'Sylph': ['S'], 'Geneticist': ['Ch', 'S'], 'Jester': ['Ch', 'K'],} 
ArchiveSpecies = {'Shinigami': 'Unearthly', 'Glitch': 'Ethereal', 'Fate': 'Unearthly', 'Herald': 'Arcane', 'Inevitable': 'Unearthly', 'Spinster': 'Unearthly', 'Hermit': 'Human', 'Harbinger': 'Unearthly', 'Emissary': 'Human', 'Page': 'Human', 'Rōjinbi': 'Ethereal', 'Philanthropist': 'Human', 'Merchant': 'Human', 'Maid': 'Human', 'Understudy': 'Human', 'Sylph': 'Ethereal',  'Geneticist': 'Human', 'Jester': 'Ethereal',}  
ArchiveFactions = {'Shinigami': [], 'Glitch': [], 'Fate': ['Coven', 'Witches'], 'Herald': [], 'Inevitable': ['Coven', 'Witches'], 'Spinster': ['Coven', 'Witches'], 'Hermit': [], 'Harbinger': ['Prophets'], 'Emissary': ['Prophets'], 'Page': ['School'], 'Guide': ['School'], 'Rōjinbi': [], 'Conduit': [], 'Philanthropist': [], 'Merchant': [], 'Maid': [], 'Understudy': [], 'Speedster': [], 'Sylph': [], 'Geneticist': [], 'Jester': [],} 
ArchiveModifiers = ['Guide', 'Conduit', 'Speedster'] 
ArchiveAchievableRoles = ['Herald', 'Inevitable', 'Spinster', 'Emissary'] 
ArchiveUniqueRoles = ['Shinigami', 'Glitch', 'Fate', 'Inevitable', 'Spinster', 'Hermit', 'Harbinger', 'Page', 'Rōjinbi', 'Philanthropist', 'Merchant', 'Maid', 'Understudy', 'Sylph', 'Geneticist', ] 
ArchiveAchievableModifiers = ['Guide'] 


ArchiveSectionsOfRoleList = [
"[+][Glitch] - Neutral, Chaos, Ethereal, Unique - <w.roles_glitch>",
"[+][Shinigami] - Evil, Counteractive/Killing, Unearthly, Unique - <w.roles_shinigami>",
"[+][Fate] - Neutral, Chaos/Support, Unearthly, Unique - <w.roles_fate>",
"[+][Herald] - Neutral, Chaos/Killing, Arcane, Unique, Achievable - <w.roles_herald>",
"[+][Inevitable] - Neutral, Killing, Unearthly, Unique, Achievable - <w.roles_inevitable>",
"[+][Spinster] - Neutral, Support, Unearthly, Unique, Achievable - <w.roles_spinster>",
"[+][Hermit] - Neutral, Investigative/Counteractive, Human, Unique - <w.roles_hermit>",
"[+][Harbinger] - Neutral, Chaos/Killing, Unearthly, Unique - <w.roles_harbinger>",
"[+][Emissary] - Neutral, Protective/Support, Human, Achievable - <w.roles_emissary>",
"[+][Page] - Neutral, Support, Human, Unique - <w.roles_page>",
"[+][Guide] - Modifier, Achievable - <w.roles_guide>",
"[+][Rōjinbi] - Neutral, Chaos, Ethereal, Unique - <w.roles_rojinbi>",
"[+][Conduit] - Modifier - <w.roles_conduit>",
"[+][Philanthropist] - Neutral, Counteractive/Support, Human, Unique - <w.roles_philanthropist>",
"[+][Merchant] - Neutral, Chaos/Support, Human, Unique - <w.roles_merchant>",
"[+][Maid] - Neutral, Chaos/Killing, Human, Unique - <w.roles_maid>",
"[+][Understudy] - Neutral, Chaos/Support, Human, Unique - <w.roles_understudy>",
"[+][Speedster] - Modifier - <w.roles_speedster>"]