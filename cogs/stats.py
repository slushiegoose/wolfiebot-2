import discord
from discord.ext import commands

from cogs.lib.gsheets import gsheets


class Stats(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.gsheets = gsheets

    @commands.command()
    async def stats(self, ctx, user: discord.Member = None):
        if not user:
            user = ctx.author
        async with ctx.typing():
            async with self.gsheets:
                d = await self.gsheets.get_user(user.id)
                if not d:
                    return await ctx.send(
                        "There is no information for that user!"
                    )
                colour = (
                    user.colour if user.colour != discord.Colour.default()
                    else discord.Embed.Empty
                )
                embed = discord.Embed(
                    title=user.display_name,
                    colour=colour
                ) \
                    .add_field(
                        name="All Time",
                        value=(
                            f'**Wins:** {d["WINS"]}\n'
                            f'**Losses:** {d["LOSSES"]}\n'
                            f'**Score:** {d["SCORE"]}\n'
                            f'**Rank:** {d["AT RANK"]}'
                        )
                    ) \
                    .add_field(
                        name="Monthly",
                        value=(
                            f'**Wins:** {d["M WINS"]}\n'
                            f'**Losses:** {d["M LOSSES"]}\n'
                            f'**Score:** {d["M SCORE"]}\n'
                            f'**Rank:** {d["M RANK"]}'
                        )
                    )
                if d["GAMES GMED"]:
                    embed.add_field(
                        name="Games Played & GMed",
                        value=(
                            f'**Games:** {d["GAMES PLAYED"]}\n'
                            f'**Rank:** {d["GP RANK"]}\n'
                            f'**GMed:** {d["GAMES GMED"]}\n'
                            f'**Rank:** {d["GM RANK"]}'
                        )
                    )
                else:
                    embed.add_field(
                        name="Games Played",
                        value=(
                            f'**Games:** {d["GAMES PLAYED"]}\n'
                            f'**Rank:** {d["GP RANK"]}'
                        )
                    )
                await ctx.send(embed=embed)



def setup(bot):
    bot.add_cog(Stats(bot))
