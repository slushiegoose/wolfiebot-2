import discord
from discord.ext import commands
from cogs.lib.checks import GM


class ChanGen(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["twins"])
    @GM
    async def twin(self, ctx, p1: discord.Member, p2: discord.Member, geneticist: discord.Member = None):
        guild = ctx.message.guild
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        twin_perms = discord.PermissionOverwrite(read_messages=True)
        overwrites = {
            guild.default_role: everyone_perms,
            discord.utils.get(guild.roles, name="Game Master"): twin_perms,
            p1: twin_perms, p2: twin_perms
        }
        if geneticist:
            overwrites[geneticist] = twin_perms
        category = discord.utils.get(guild.categories, name="Priv Channels")
        chan = await guild.create_text_channel(
            "twins", overwrites=overwrites, category=category)
        return chan

    @commands.command()
    @GM
    async def tardis(
        self,
        ctx,
        timelord: discord.Member,
        companion: discord.Member
    ):
        guild = ctx.message.guild
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        crew_perms = discord.PermissionOverwrite(read_messages=True)
        tardises = [c for c in guild.channels if c.name == "tardis"]
        if [
            c for c in tardises
            if c.permissions_for(timelord).read_messages
        ] == []:
            overwrites = {
                guild.default_role: everyone_perms,
                discord.utils.get(guild.roles, name="Game Master"): crew_perms,
                timelord: crew_perms}
            category = discord.utils.get(
                guild.categories, name="Priv Channels"
            )
            tardis_channel = await guild.create_text_channel(
                "tardis", overwrites=overwrites, category=category
            )
        else:
            tardis_channel = [
                c for c in tardises
                if c.permissions_for(timelord).read_messages
            ][0]
        if not tardis_channel.permissions_for(companion).read_messages:
            await tardis_channel.set_permissions(
                companion, overwrite=crew_perms
            )
        else:
            await tardis_channel.set_permissions(companion, overwrite=None)
        return tardis_channel

    @commands.command()
    @GM
    async def coven(self, ctx):
        guild = ctx.message.guild
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        fate_perms = discord.PermissionOverwrite(read_messages=True)
        coven_channel = discord.utils.get(guild.channels, name="coven")
        if coven_channel is None:
            overwrites = {
                guild.default_role: everyone_perms,
                discord.utils.get(guild.roles, name="Game Master"): fate_perms
            }
            category = discord.utils.get(
                guild.categories,
                name="Priv Channels"
            )
            coven_channel = await guild.create_text_channel(
                "coven", overwrites=overwrites, category=category
            )
        players = ctx.message.mentions
        for p in players:
            await coven_channel.set_permissions(p, overwrite=fate_perms)
        return coven_channel

    @commands.command(aliases=["w","wolf"])
    @GM
    async def wolves(self, ctx):
        guild = ctx.message.guild
        wolf_perms = discord.PermissionOverwrite(read_messages=True)
        wolves_channel = discord.utils.get(guild.channels, name="wolves")
        if wolves_channel is None:
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            overwrites = {
                guild.default_role: everyone_perms,
                discord.utils.get(guild.roles, name="Game Master"): wolf_perms
            }
            category = discord.utils.get(
                guild.categories, name="Priv Channels"
            )
            wolves_channel = await guild.create_text_channel(
                "wolves", overwrites=overwrites, category=category
            )
        players = ctx.message.mentions
        for p in players:
            await wolves_channel.set_permissions(p, overwrite=wolf_perms)
        return wolves_channel

    @commands.command()
    @GM
    async def seance(
        self,
        ctx,
        medium: discord.Member,
        target: discord.Member
    ):
        guild = ctx.message.guild
        everyone_perms = discord.PermissionOverwrite(read_messages=False)
        seance_perms = discord.PermissionOverwrite(read_messages=True)
        overwrites = {
            guild.default_role: everyone_perms,
            discord.utils.get(guild.roles, name="Game Master"): seance_perms,
            medium: seance_perms, target: seance_perms
        }
        category = discord.utils.get(guild.categories, name="Priv Channels")
        chan = await guild.create_text_channel(
            "seance", overwrites=overwrites, category=category
        )
        return chan

    @commands.command(aliases=["v"])
    @GM
    async def vampires(self, ctx):
        guild = ctx.message.guild
        vampire_perms = discord.PermissionOverwrite(read_messages=True)
        vampires_channel = discord.utils.get(guild.channels, name="vampires")
        if vampires_channel is None:
            everyone_perms = discord.PermissionOverwrite(read_messages=False)
            overwrites = {
                guild.default_role: everyone_perms,
                discord.utils.get(guild.roles, name="Game Master"):
                vampire_perms
            }
            category = discord.utils.get(
                guild.categories, name="Priv Channels"
            )
            vampires_channel = await guild.create_text_channel(
                "vampires", overwrites=overwrites, category=category
            )
        players = ctx.message.mentions
        for p in players:
            await vampires_channel.set_permissions(p, overwrite=vampire_perms)
        return vampires_channel


def setup(bot):
    bot.add_cog(ChanGen(bot))
