import discord
from discord.ext import commands
from cogs.lib.checks import GM
import random
from cogs.lib import AllRoles
from cogs.lib.channels import voting, game


class RoleEffects(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @GM
    async def medium(self, ctx, user: discord.Member, status="t"):
        dead_channel = self.bot.get_channel(510935117926236160)
        status = status.lower()
        if status == "t" or status == "true":
            perms = discord.PermissionOverwrite()
            perms.read_messages = True
            await dead_channel.set_permissions(user, overwrite=perms)
        elif status == "f" or status == "false":
            await dead_channel.set_permissions(user, overwrite=None)

    @commands.command()
    @GM
    async def sonic(self, ctx, *, roles=""):
        if roles == "":
            await ctx.send("""Usage of command <w.sonic>:
```md
<w.sonic (comma-seperated list of all roles in game)>
Example: <w.sonic Direwolf, Werewolf, Seer, Jailor>
Output: 'Direwolf, Drunk, Jailor, Shifter'```""")
            return None
        roles = roles.split(", ")
        random.shuffle(roles)
        sonic = []
        All = [r for r in AllRoles if r not in [x.title() for x in roles]]
        for i in range(0, round(len(roles)/2)):
            sonic.append(roles[i])
        for i in range(0, round((len(roles)-0.5)/2)):
            sonic.append(random.choice(All))
        sonic = sorted(sonic)
        output = sonic[0]
        for i in range(1, len(sonic)):
            output = "{}, {}".format(output, sonic[i])
        await ctx.send(output)

    @commands.command()
    @GM
    async def lockjaw(self, ctx, user: discord.Member, status="t"):
        game_channel = self.bot.get_channel(game)
        voting_channel = self.bot.get_channel(voting)
        status = status.lower()
        if status == "t" or status == "true":
            gperms = discord.PermissionOverwrite()
            vperms = discord.PermissionOverwrite()
            gperms.send_messages = False
            gperms.add_reactions = False
            vperms.add_reactions = False
            await game_channel.set_permissions(user, overwrite=gperms)
            await voting_channel.set_permissions(user, overwrite=vperms)
        elif status == "f" or status == "false":
            await game_channel.set_permissions(user, overwrite=None)
            await voting_channel.set_permissions(user, overwrite=None)


def setup(bot):
    bot.add_cog(RoleEffects(bot))
