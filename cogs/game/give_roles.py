import discord
from discord.ext import commands
from cogs.lib.checks import GM
import asyncio


class GiveRoles(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["m"])
    @GM
    async def mayor(self, ctx, player: discord.Member):
        player_role = discord.utils.get(ctx.message.guild.roles, name="Player")
        mayor_role = discord.utils.get(ctx.message.guild.roles, name="Mayor")
        deputy_role = discord.utils.get(ctx.message.guild.roles, name="Deputy")
        if "Player" in [y.name for y in player.roles]:
            if mayor_role not in [x for y in [
                p.roles for p in ctx.message.guild.members
                if player_role in p.roles
            ] for x in y]:
                await player.add_roles(mayor_role)
                if deputy_role in [y for y in player.roles]:
                    await player.remove_roles(deputy_role)
            else:
                await ctx.send("There is already a Mayor in this game.")
        else:
            await ctx.send("User is either dead or not in the game.")

    @commands.command(aliases=["dep","d"])
    @GM
    async def deputy(self, ctx, player: discord.Member, invoked=False):
        player_role = discord.utils.get(ctx.message.guild.roles, name="Player")
        mayor_role = discord.utils.get(ctx.message.guild.roles, name="Mayor")
        deputy_role = discord.utils.get(ctx.message.guild.roles, name="Deputy")
        if "Player" in [y.name for y in player.roles]:
            if deputy_role not in [x for y in [
                p.roles for p in ctx.message.guild.members
                if player_role in p.roles
            ] for x in y]:
                if mayor_role not in [y for y in player.roles]:
                    await player.add_roles(deputy_role)
                else:
                    await ctx.send("User is the Mayor.")
            else:
                await ctx.send("There is already a Deputy in this game.")
        else:
            await ctx.send("User is either dead or not in the game.")

    @commands.command(aliases=["k"])
    @GM
    async def kill(self, ctx):
        player_role = discord.utils.get(ctx.message.guild.roles, name="Player")
        dead_role = discord.utils.get(ctx.message.guild.roles, name="Dead")
        mentions = ctx.message.mentions
        for player in mentions:
            if "Player" in [y.name for y in player.roles]:
                await player.add_roles(dead_role)
                await asyncio.sleep(1)
                await player.remove_roles(player_role)
                if "Mayor" in [y.name for y in player.roles]:
                    await asyncio.sleep(1)
                    mayor_role = discord.utils.get(
                        ctx.message.guild.roles, name="Mayor"
                    )
                    await player.remove_roles(mayor_role)
                if "Deputy" in [y.name for y in player.roles]:
                    await asyncio.sleep(1)
                    deputy_role = discord.utils.get(
                        ctx.message.guild.roles, name="Deputy"
                    )
                    await player.remove_roles(deputy_role)
                await asyncio.sleep(1)
                await ctx.invoke(
                    self.bot.get_command("lockjaw"), user=player, status="f"
                )
                await ctx.invoke(
                    self.bot.get_command("medium"), user=player, status="f"
                )
            else:
                name = player.nick
                if name is None:
                    name = player.name
                await ctx.send(
                    "**{}** is either already dead or not in the game."
                    .format(name)
                )

    @commands.command(aliases=["rev"])
    @GM
    async def revive(self, ctx, player: discord.Member):
        player_role = discord.utils.get(ctx.message.guild.roles, name="Player")
        dead_role = discord.utils.get(ctx.message.guild.roles, name="Dead")
        mentions = ctx.message.mentions
        for player in mentions:
            if "Dead" in [y.name for y in player.roles]:
                await player.add_roles(player_role)
                await asyncio.sleep(1)
                await player.remove_roles(dead_role)
                await asyncio.sleep(1)
            else:
                name = player.nick
                if name is None:
                    name = player.name
                await ctx.send(
                    "**{}** is either already alive or not in the game."
                    .format(name)
                )


def setup(bot):
    bot.add_cog(GiveRoles(bot))
