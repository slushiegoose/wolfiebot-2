import sys
from io import StringIO
import discord
from discord.ext import commands, tasks


class Redirect(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.stream = StringIO()
        self.channel = bot.get_channel(511239399934132225)
        sys.stdout = self.stream
        sys.stderr = self.stream
        self.loop.start()

    def cog_unload(self):
        self.loop.stop()
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__
    
    @tasks.loop(seconds=0.2)
    async def loop(self):
        stream_content = self.stream.getvalue()
        if stream_content:
            out = '```diff\n'
            j = stream_content.split('\n')
            for k in j:
                l = '-'+k+'\n'
                if len(out+l+'```') > 2000:
                    await self.channel.send(out+'```')
                    out = '```diff\n'
                out += l
            await self.channel.send(out+'```')
            self.stream = StringIO()
            sys.stdout = self.stream
            sys.stderr = self.stream

def setup(bot):
    bot.add_cog(Redirect(bot))