#!/bin/bash
export PYTHONIOENCODING=utf8
source /root/.profile
pyenv shell 3.6.5
git pull
while [ -f restart.txt ]
do
    git pull
    pipenv sync
    pipenv run start
done
